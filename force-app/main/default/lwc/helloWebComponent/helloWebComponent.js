import { LightningElement, track } from "lwc";

export default class HelloWebComponent extends LightningElement {
  // Annotations
  // track - local reactive variable
  // API - global reactive variable
  // wire - quick connection to salesforce objects

  // local reactive variable
  greeting = "Trailblazer"; // primitive values string, integer, decimal, float ect...
  @track trackVar = {};

  currentDate = new Date().toDateString();

  // getter method
  get capitalizedGreeting() {
    return `Hello ${this.greeting.toUpperCase()}!`;
  }

  handleGreetingChange(event) {
    this.greeting = event.target.value;
  }
}